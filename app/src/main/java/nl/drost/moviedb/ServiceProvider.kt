package nl.drost.moviedb

import android.content.Context
import android.graphics.Bitmap
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.squareup.moshi.Moshi
import com.squareup.moshi.adapters.Rfc3339DateJsonAdapter
import com.squareup.picasso.OkHttp3Downloader
import com.squareup.picasso.Picasso
import nl.drost.moviedb.api.ApiService
import nl.drost.moviedb.data.*
import okhttp3.Cache
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import timber.log.Timber
import java.io.File
import java.util.*
import java.util.concurrent.TimeUnit

interface Provider {
    fun getMovieApiService(): ApiService
    fun getOkHttpClient(): OkHttpClient
    fun getPicassoClient(): Picasso
    fun getPopularMovieRepository(): Repository<PopularMovieModel>
    fun getMovieRepository(): Repository<MovieModel>
}

internal class ServiceProvider(private val applicationContext: Context) : Provider {

    private val moshi: Moshi by lazy {
        Moshi.Builder()
            .add(Date::class.java, Rfc3339DateJsonAdapter().nullSafe())
            .build()
    }

    private val apiService: ApiService by lazy {
        Retrofit.Builder()
            .baseUrl("https://api.themoviedb.org/3/")
            .client(okhttp)
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .build().create(ApiService::class.java)
    }

    private val okhttp by lazy {
        val builder = OkHttpClient.Builder()

        builder.readTimeout(60, TimeUnit.SECONDS)

        val authInterceptor = Interceptor { chain ->
            val httpUrl =
                chain.request().url().newBuilder().addQueryParameter("api_key", "1d51cef38e888586924becd5f1cc9b1d")
                    .build()
            val request = chain.request().newBuilder().url(httpUrl).build()

            chain.proceed(request)
        }

        builder.addInterceptor(authInterceptor)

        if (BuildConfig.DEBUG) {
            val logger = HttpLoggingInterceptor { message ->
                Timber.tag("OkHttp")
                Timber.d(message)
            }
            logger.level = HttpLoggingInterceptor.Level.BODY
            builder.addInterceptor(logger)
        }

        val cacheFolderSize: Long = 1024 * 1024 * 50
        val cache = Cache(File(applicationContext.cacheDir, "requests"), cacheFolderSize)

        builder.cache(cache)

        builder.build()
    }

    private val picasso by lazy {
        val picasso = Picasso.Builder(applicationContext)
            .downloader(OkHttp3Downloader(this.okhttp))
            .defaultBitmapConfig(Bitmap.Config.RGB_565)
            .indicatorsEnabled(BuildConfig.DEBUG)
            .build()

        Picasso.setSingletonInstance(picasso)
        picasso
    }

    override fun getMovieApiService(): ApiService = apiService

    override fun getOkHttpClient(): OkHttpClient = okhttp

    override fun getPicassoClient(): Picasso = picasso

    override fun getPopularMovieRepository() = PopularMovieRepository(apiService)

    override fun getMovieRepository() = MovieRepository(apiService)
}