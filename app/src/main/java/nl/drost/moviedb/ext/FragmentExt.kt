package nl.drost.moviedb.ext

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import nl.drost.moviedb.ViewModelFactory

inline fun <reified T : ViewModel?> Fragment.viewModel() =
    lazy { ViewModelProviders.of(this, ViewModelFactory(requireContext())).get(T::class.java) }

inline fun <reified T : ViewModel?> Fragment.sharedViewModel() =
    lazy { ViewModelProviders.of(requireActivity(), ViewModelFactory(requireContext())).get(T::class.java) }