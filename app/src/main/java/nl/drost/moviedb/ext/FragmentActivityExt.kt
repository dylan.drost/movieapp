package nl.drost.moviedb.ext

import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import nl.drost.moviedb.ViewModelFactory

inline fun <reified T : ViewModel?> FragmentActivity.viewModel() =
    lazy { ViewModelProviders.of(this, ViewModelFactory(this.applicationContext)).get(T::class.java) }