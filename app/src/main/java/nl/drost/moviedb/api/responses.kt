package nl.drost.moviedb.api

import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@JsonClass(generateAdapter = true)
data class ResultCollection<T>(
    @field:Json(name = "page") val page: Int,
    @field:Json(name = "results") val results: List<T>,
    @field:Json(name = "total_results") val totalResults: Int,
    @field:Json(name = "total_pages") val totalPages: Int
)

@JsonClass(generateAdapter = true)
data class PopularMovie(
    @field:Json(name = "id") val id: Int,
    @field:Json(name = "original_title") val originalTitle: String,
    @field:Json(name = "title") val title: String,
    @field:Json(name = "overview") val overview: String,
    @field:Json(name = "release_date") val releaseDate: String,
    @field:Json(name = "genre_ids") val genreIds: List<Int>,
    @field:Json(name = "poster_path") val posterPath: String?,
    @field:Json(name = "backdrop_path") val backdropPath: String?
)

@JsonClass(generateAdapter = true)
data class Movie(
    @field:Json(name = "id") val id: Int,
    @field:Json(name = "original_title") val originalTitle: String,
    @field:Json(name = "title") val title: String,
    @field:Json(name = "overview") val overview: String,
    @field:Json(name = "release_date") val releaseDate: String,
    @field:Json(name = "genres") val genres: List<Genre>,
    @field:Json(name = "poster_path") val posterPath: String?,
    @field:Json(name = "backdrop_path") val backdropPath: String?,
    @field:Json(name = "tagline") val tagline: String?,
    @field:Json(name = "runtime") val runtime: Int?,
    @field:Json(name = "credits") val credits: Credits
)

@Parcelize
@JsonClass(generateAdapter = true)
data class Config(
    @field:Json(name = "images") val images: Images,
    @field:Json(name = "change_keys") val changeKeys: List<String>
) : Parcelable

@Parcelize
@JsonClass(generateAdapter = true)
data class Images(
    @field:Json(name = "base_url") val baseUrl: String,
    @field:Json(name = "secure_base_url") val secureBaseUrl: String,
    @field:Json(name = "backdrop_sizes") val backDropSizes: List<String>,
    @field:Json(name = "logo_sizes") val logoSizes: List<String>,
    @field:Json(name = "poster_sizes") val posterSizes: List<String>,
    @field:Json(name = "still_sizes") val stillSizes: List<String>
) : Parcelable

@Parcelize
@JsonClass(generateAdapter = true)
data class Genres(
    @field:Json(name = "genres") val genres: List<Genre>
) : Parcelable

@Parcelize
@JsonClass(generateAdapter = true)
data class Genre(
    @field:Json(name = "id") val id: Int,
    @field:Json(name = "name") val name: String
) : Parcelable

@JsonClass(generateAdapter = true)
data class Credits(@field:Json(name = "cast") val cast: List<Cast>, @field:Json(name = "crew") val crew: List<Crew>)

@JsonClass(generateAdapter = true)
data class Cast(
    @field:Json(name = "id") val id: Int,
    @field:Json(name = "cast_id") val castId: Int,
    @field:Json(name = "credit_id") val creditId: String,
    @field:Json(name = "character") val character: String,
    @field:Json(name = "name") val name: String,
    @field:Json(name = "profile_path") val profilePath: String?,
    @field:Json(name = "order") val order: Int
) {
    val profileUrl: String
        get() = "https://image.tmdb.org/t/p/w185$profilePath"
}

@JsonClass(generateAdapter = true)
data class Crew(
    @field:Json(name = "id") val id: Int,
    @field:Json(name = "credit_id") val creditId: String,
    @field:Json(name = "job") val job: String,
    @field:Json(name = "name") val name: String,
    @field:Json(name = "profile_path") val profilePath: String?,
    @field:Json(name = "department") val department: String
)
