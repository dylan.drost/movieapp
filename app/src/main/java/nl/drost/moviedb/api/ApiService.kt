package nl.drost.moviedb.api

import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {
    @GET("movie/{id}")
    fun getMovieDetails(@Path("id") id: Int, @Query("append_to_response") append: String = "credits"): Deferred<Movie>

    @GET("movie/popular")
    fun getPopularMovies(@Query("page") page: Int = 1): Deferred<ResultCollection<PopularMovie>>

    @GET("configuration")
    fun getConfig(): Deferred<Config>

    @GET("genre/movie/list")
    fun getGenres(): Deferred<Genres>
}