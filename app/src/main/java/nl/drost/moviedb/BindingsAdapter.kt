package nl.drost.moviedb

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import nl.drost.moviedb.api.Genre

object BindingsAdapter {
    @JvmStatic
    @BindingAdapter("imageUrl", "circleCrop", requireAll = false)
    fun bindImageUrl(
        view: ImageView,
        url: String? = null,
        circleCrop: Boolean
    ) {
        val picassoClient = (view.context.applicationContext as MovieApplication).serviceProvider.getPicassoClient()
        val picasso = picassoClient.load(url)

        if (circleCrop && url != null) {
            picasso.transform(CircleCropTransformation(url))
        }

        picasso.tag(view.context).fit().into(view)
    }

    @JvmStatic
    @BindingAdapter("show", "keepInLayout", requireAll = false)
    fun show(view: View, value: Any?, keepInLayout: Boolean?) {
        val hideVisibility = if (keepInLayout == true) View.INVISIBLE else View.GONE
        if (value is Boolean) {
            view.visibility = if (value) View.VISIBLE else hideVisibility
        } else if (value != null && "" != (value)) {
            view.visibility = View.VISIBLE
        } else {
            view.visibility = hideVisibility
        }
    }

    @JvmStatic
    @BindingAdapter("hide", "keepInLayout", requireAll = false)
    fun hide(view: View, value: Any?, keepInLayout: Boolean?) {
        val hideVisibility = if (keepInLayout == true) View.INVISIBLE else View.GONE
        if (value is Boolean) {
            view.visibility = if (value) hideVisibility else View.VISIBLE
        } else if (value == null || "" == value) {
            view.visibility = hideVisibility
        } else {
            view.visibility = View.VISIBLE
        }
    }

    @JvmStatic
    @BindingAdapter("genres")
    fun bindGenres(view: TextView, genres: List<Genre>?) {
        if (genres != null) {
            val concatenatedString = genres.fold("", operation = { acc, genre ->
                if (acc.isEmpty()) {
                    acc + genre.name
                } else {
                    acc + ", ${genre.name}"
                }
            })
            view.text = concatenatedString
        }
    }
}