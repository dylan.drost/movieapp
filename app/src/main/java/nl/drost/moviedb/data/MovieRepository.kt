package nl.drost.moviedb.data

import nl.drost.moviedb.api.ApiService
import nl.drost.moviedb.api.Config
import nl.drost.moviedb.api.Movie

class MovieRepository(private val apiService: ApiService) : Repository<MovieModel> {
    override suspend fun get(id: Int): MovieModel {
        val movieRequest = apiService.getMovieDetails(id)
        val configRequest = apiService.getConfig()

        return mapToModel(movieRequest.await(), configRequest.await())
    }

    private fun mapToModel(movie: Movie, config: Config): MovieModel {
        return MovieModel(
            movie.id,
            movie.title,
            movie.overview,
            movie.genres,
            movie.runtime,
            movie.posterPath,
            movie.backdropPath,
            movie.credits,
            config
        )
    }
}