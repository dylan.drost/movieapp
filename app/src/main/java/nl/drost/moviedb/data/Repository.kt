package nl.drost.moviedb.data

interface Repository<T> {
    suspend fun get(id: Int): T {
        throw RuntimeException("This function is not implemented for this instance")
    }

    suspend fun getList(): List<T> {
        throw RuntimeException("This function is not implemented for this instance")
    }

    suspend fun getList(offset: Int): List<T> {
        throw RuntimeException("This function is not implemented for this instance")
    }
}