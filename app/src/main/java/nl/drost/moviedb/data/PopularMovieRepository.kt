package nl.drost.moviedb.data

import nl.drost.moviedb.api.*

class PopularMovieRepository(private val apiService: ApiService) : Repository<PopularMovieModel> {

    override suspend fun getList(): List<PopularMovieModel> {
        try {
            val movieRequest = apiService.getPopularMovies()
            val configRequest = apiService.getConfig()
            val genresRequest = apiService.getGenres()

            return mapResults(movieRequest.await().results, configRequest.await(), genresRequest.await())
        } catch (error: Exception) {
            throw error
        }
    }

    private fun mapResults(popularMovies: List<PopularMovie>, config: Config, genres: Genres): List<PopularMovieModel> {
        return popularMovies.map { mapMovieToModel(it, config, genres.genres) }
    }

    private fun mapMovieToModel(popularMovie: PopularMovie, config: Config, genres: List<Genre>): PopularMovieModel {
        val movieGenres = genres.filter { popularMovie.genreIds.contains(it.id) }

        return PopularMovieModel(
            popularMovie.id,
            popularMovie.title,
            popularMovie.overview,
            movieGenres,
            popularMovie.posterPath,
            popularMovie.backdropPath,
            config
        )
    }
}