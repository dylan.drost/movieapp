package nl.drost.moviedb.data

import nl.drost.moviedb.api.Cast
import nl.drost.moviedb.api.Config
import nl.drost.moviedb.api.Credits
import nl.drost.moviedb.api.Genre

data class MovieModel(
    val id: Int,
    val title: String,
    val overview: String,
    val genres: List<Genre>,
    val runtime: Int?,
    private val posterPath: String?,
    private val backdropPath: String?,
    private val credits: Credits?,
    private val config: Config
) {
    companion object {
        fun fromPopularMovie(movie: PopularMovieModel): MovieModel {
            return MovieModel(
                movie.id,
                movie.title,
                movie.overview,
                movie.genres,
                null,
                movie.posterPath,
                movie.backdropPath,
                null,
                movie.config
            )
        }
    }

    val posterUrl: String?
        get() {
            if (posterPath == null) {
                return null
            }
            return "${config.images.secureBaseUrl}${config.images.posterSizes[1]}$posterPath"
        }

    val backdropUrl: String?
        get() {
            if (backdropPath == null) {
                return null
            }
            return "${config.images.secureBaseUrl}${config.images.backDropSizes[2]}$backdropPath"
        }

    val cast: List<Cast>
        get() {
            return credits?.cast ?: emptyList()
        }

    val director: String
        get() {
            val director = credits?.crew?.find { it.job.toLowerCase() == "director" }
            return director?.name ?: "Unknown"
        }
}