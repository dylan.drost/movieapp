package nl.drost.moviedb.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import nl.drost.moviedb.api.Config
import nl.drost.moviedb.api.Genre

@Parcelize
class PopularMovieModel(
    val id: Int,
    val title: String,
    val overview: String,
    val genres: List<Genre>,
    internal val posterPath: String?,
    internal val backdropPath: String?,
    internal val config: Config
) : Parcelable {
    val posterUrl: String?
        get() {
            if (posterPath == null) {
                return null
            }
            return "${config.images.secureBaseUrl}${config.images.posterSizes[1]}$posterPath"
        }

    val backdropUrl: String?
        get() {
            if (backdropPath == null) {
                return null
            }
            return "${config.images.secureBaseUrl}${config.images.backDropSizes[2]}$backdropPath"
        }
}