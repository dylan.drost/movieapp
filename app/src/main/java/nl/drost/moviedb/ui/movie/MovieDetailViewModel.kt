package nl.drost.moviedb.ui.movie

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.launch
import nl.drost.moviedb.BaseViewModel
import nl.drost.moviedb.data.MovieModel
import nl.drost.moviedb.data.PopularMovieModel
import nl.drost.moviedb.data.Repository
import retrofit2.HttpException
import timber.log.Timber
import java.io.IOException

class MovieDetailViewModel(private val repository: Repository<MovieModel>) : BaseViewModel() {
    val state: LiveData<State> = MutableLiveData()

    fun setMovie(popularMovie: PopularMovieModel) {
        Timber.e("set movie: ${popularMovie.id}")
        val movie = MovieModel.fromPopularMovie(popularMovie)
        (state as MutableLiveData).postValue(State.ResultLoading(movie))
        loadMovie(popularMovie.id)
    }

    private fun loadMovie(id: Int) {
        launch(coroutineContext) {
            val state = try {
                Timber.e("load movie: $id")
                val movie = repository.get(id)
                State.Result(movie)
            } catch (error: Exception) {
                Timber.e(error)
                when (error) {
                    is HttpException -> State.Error(error.message())
                    is IOException -> State.Error(error.localizedMessage)
                    else -> State.Error(error.localizedMessage)
                }
            }
            (this@MovieDetailViewModel.state as MutableLiveData).postValue(state)
        }
    }

    sealed class State {
        object Loading : State()
        data class Error(val msg: String) : State()
        data class Result(val movie: MovieModel) : State()
        data class ResultLoading(val movie: MovieModel) : State()
    }
}