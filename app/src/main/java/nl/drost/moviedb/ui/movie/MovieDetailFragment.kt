package nl.drost.moviedb.ui.movie

import android.graphics.Rect
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.Section
import com.xwray.groupie.databinding.ViewHolder
import nl.drost.moviedb.R
import nl.drost.moviedb.databinding.FragmentMovieBinding
import nl.drost.moviedb.ext.sharedViewModel
import timber.log.Timber

class MovieDetailFragment : Fragment() {
    companion object {
        fun newInstance() = MovieDetailFragment()
    }

    private lateinit var binding: FragmentMovieBinding
    private val viewModel: MovieDetailViewModel by sharedViewModel()

    private val section: Section = Section()
    private val adapter: GroupAdapter<ViewHolder<*>> = GroupAdapter<ViewHolder<*>>().apply { add(section) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel.state.observe(this, Observer {
            Timber.e("Observer State")
            when (it) {
                is MovieDetailViewModel.State.ResultLoading -> {
                    Timber.e("Observer loading result")
                    binding.movie = it.movie
                    val castList = it.movie.cast.map { CastItem(it) }
                    section.update(castList)
                }
                is MovieDetailViewModel.State.Result -> {
                    Timber.e("Observer result")
                    binding.movie = it.movie
                    val castList = it.movie.cast.map { CastItem(it) }
                    section.update(castList)
                }
            }
        })
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMovieBinding.inflate(inflater, container, false)
        binding.cast.setHasFixedSize(true)
        binding.cast.adapter = adapter
        binding.cast.addItemDecoration(object : RecyclerView.ItemDecoration() {
            override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
                super.getItemOffsets(outRect, view, parent, state)

                val dimensionPixelSize = view.context.resources.getDimensionPixelSize(R.dimen.cast_spacing)
                when (parent.getChildAdapterPosition(view)) {
                    0 -> {
                        outRect.right = dimensionPixelSize
                        outRect.left = dimensionPixelSize * 2
                    }
                    parent.childCount - 1 -> {
                        outRect.right = dimensionPixelSize * 2
                        outRect.left = dimensionPixelSize
                    }
                    else -> {
                        outRect.right = dimensionPixelSize
                        outRect.left = dimensionPixelSize
                    }

                }
            }
        })

        Timber.e("binding created")
        return binding.root
    }


}
