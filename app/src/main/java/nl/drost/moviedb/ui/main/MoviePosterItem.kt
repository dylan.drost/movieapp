package nl.drost.moviedb.ui.main

import android.view.View
import com.xwray.groupie.databinding.BindableItem
import nl.drost.moviedb.R
import nl.drost.moviedb.data.PopularMovieModel
import nl.drost.moviedb.databinding.ItemMoviePosterBinding

class MoviePosterItem(
    private val movie: PopularMovieModel,
    private val onclick: (popularMovie: PopularMovieModel) -> Unit
) : BindableItem<ItemMoviePosterBinding>(movie.id.toLong()) {
    override fun getLayout() = R.layout.item_movie_poster

    override fun bind(viewBinding: ItemMoviePosterBinding, position: Int) {
        viewBinding.movie = movie
        viewBinding.onclick = View.OnClickListener {
            onclick.invoke(movie)
        }
    }

    override fun isClickable() = true
}