package nl.drost.moviedb.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.launch
import nl.drost.moviedb.BaseViewModel
import nl.drost.moviedb.data.PopularMovieModel
import nl.drost.moviedb.data.Repository
import retrofit2.HttpException
import timber.log.Timber
import java.io.IOException

class MainViewModel(private val repository: Repository<PopularMovieModel>) : BaseViewModel() {

    val state: LiveData<State> = MutableLiveData<State>().apply { value = State.Loading }

    fun loadMovies() {
        launch(coroutineContext) {
            val localData = try {
                val list = repository.getList()
                createState(list, state.value)
            } catch (error: Exception) {
                Timber.e(error)
                when (error) {
                    is HttpException -> State.Error(error.message())
                    is IOException -> State.Error(error.localizedMessage)
                    else -> State.Error(error.localizedMessage)
                }
            }
            (this@MainViewModel.state as MutableLiveData<State>).postValue(localData)
        }
    }

    private fun createState(popularMovies: List<PopularMovieModel>, oldState: State?): State {
        return when (oldState) {
            is State.Loading -> State.Result(popularMovies)
            is State.Error -> State.Result(popularMovies)
            is State.Result -> State.Result(oldState.popularMovies + popularMovies)
            is State.ResultLoading -> State.Result(oldState.popularMovies + popularMovies)
            null -> State.Result(popularMovies)
        }
    }

    sealed class State {
        object Loading : State()
        data class Error(val msg: String) : State()
        data class Result(val popularMovies: List<PopularMovieModel>) : State()
        data class ResultLoading(val popularMovies: List<PopularMovieModel>) : State()
    }
}
