package nl.drost.moviedb.ui.movie

import com.xwray.groupie.databinding.BindableItem
import nl.drost.moviedb.R
import nl.drost.moviedb.api.Cast
import nl.drost.moviedb.databinding.ItemCastBinding

class CastItem(private val cast: Cast) : BindableItem<ItemCastBinding>(cast.id.toLong()) {
    override fun getLayout() = R.layout.item_cast

    override fun bind(viewBinding: ItemCastBinding, position: Int) {
        viewBinding.cast = cast
    }
}