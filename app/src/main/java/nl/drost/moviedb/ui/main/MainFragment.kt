package nl.drost.moviedb.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.Section
import com.xwray.groupie.databinding.ViewHolder
import nl.drost.moviedb.data.PopularMovieModel
import nl.drost.moviedb.databinding.MainFragmentBinding
import nl.drost.moviedb.ext.viewModel
import nl.drost.moviedb.ui.common.ErrorItem
import nl.drost.moviedb.ui.common.LoadingIndicatorItem
import nl.drost.moviedb.ui.movie.MovieDetailActivity

class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    private val viewModel: MainViewModel by viewModel()
    private lateinit var binding: MainFragmentBinding
    private val section: Section = Section()
    private val adapter: GroupAdapter<ViewHolder<*>> = GroupAdapter<ViewHolder<*>>().apply { add(section) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.state.observe(this, Observer { it ->
            when (it) {
                is MainViewModel.State.Loading -> binding.loading = true
                is MainViewModel.State.Result -> {
                    binding.loading = false
                    updateMovies(it.popularMovies)
                }
                is MainViewModel.State.ResultLoading -> {
                    binding.loading = false
                    updateMovies(it.popularMovies)
                    section.setFooter(LoadingIndicatorItem())
                }
                is MainViewModel.State.Error -> {
                    binding.loading = false
                    section.setPlaceholder(ErrorItem(it.msg) {
                        viewModel.loadMovies()
                    })
                }
            }
        })
        viewModel.loadMovies()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = MainFragmentBinding.inflate(inflater, container, false)
        binding.loading = true
        binding.grid.adapter = adapter
        binding.grid.setHasFixedSize(true)
        return binding.root
    }

    private fun updateMovies(popularMovies: List<PopularMovieModel>) {
        val movieItems = popularMovies.map { it ->
            MoviePosterItem(it, this::openMovie)
        }

        section.update(movieItems)
    }

    private fun openMovie(popularMovie: PopularMovieModel) {
        MovieDetailActivity.showMovieDetails(requireContext(), popularMovie)
    }

}
