package nl.drost.moviedb.ui.movie

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.transaction
import nl.drost.moviedb.R
import nl.drost.moviedb.data.PopularMovieModel
import nl.drost.moviedb.ext.viewModel

private var Intent.popularMovie: PopularMovieModel
    get() = this.getParcelableExtra("popularMovie")
    set(value) {
        this.putExtra("popularMovie", value)
    }

class MovieDetailActivity : AppCompatActivity() {

    companion object {
        fun showMovieDetails(context: Context, popularMovie: PopularMovieModel) {
            val intent = Intent(context, MovieDetailActivity::class.java).apply { this.popularMovie = popularMovie }
            context.startActivity(intent)
        }
    }

    val model: MovieDetailViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)

        model.setMovie(intent.popularMovie)

        if (savedInstanceState == null) {
            supportFragmentManager.transaction(now = true) {
                replace(R.id.container, MovieDetailFragment.newInstance())
            }
        }
    }
}
