package nl.drost.moviedb.ui.common

import com.xwray.groupie.databinding.BindableItem
import nl.drost.moviedb.R
import nl.drost.moviedb.databinding.ItemLoadingIndicatorBinding

class LoadingIndicatorItem : BindableItem<ItemLoadingIndicatorBinding>() {
    override fun getLayout() = R.layout.item_loading_indicator

    override fun bind(viewBinding: ItemLoadingIndicatorBinding, position: Int) {
    }
}