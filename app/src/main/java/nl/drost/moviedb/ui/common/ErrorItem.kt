package nl.drost.moviedb.ui.common

import android.view.View
import com.xwray.groupie.databinding.BindableItem
import nl.drost.moviedb.R
import nl.drost.moviedb.databinding.ItemErrorBinding

class ErrorItem(private val error: String, private val retry: () -> Unit) : BindableItem<ItemErrorBinding>() {

    override fun getLayout() = R.layout.item_error

    override fun bind(viewBinding: ItemErrorBinding, position: Int) {
        viewBinding.errorMsg = error
        viewBinding.onclick = View.OnClickListener {
            retry.invoke()
        }
    }
}