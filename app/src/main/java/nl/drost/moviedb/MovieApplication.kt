package nl.drost.moviedb

import android.app.Application
import timber.log.Timber
import timber.log.Timber.DebugTree


class MovieApplication : Application() {
    val serviceProvider: Provider = ServiceProvider(this)

    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG) {
            Timber.plant(DebugTree())
        }
    }
}