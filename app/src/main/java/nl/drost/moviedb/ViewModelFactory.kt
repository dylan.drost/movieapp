package nl.drost.moviedb

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import nl.drost.moviedb.ui.main.MainViewModel
import nl.drost.moviedb.ui.movie.MovieDetailViewModel

class ViewModelFactory(private val context: Context) : ViewModelProvider.Factory {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        val provider: Provider = (context.applicationContext as MovieApplication).serviceProvider
        return when (modelClass) {
            MainViewModel::class.java -> MainViewModel(provider.getPopularMovieRepository())
            MovieDetailViewModel::class.java -> MovieDetailViewModel(provider.getMovieRepository())
            else -> {
                throw IllegalArgumentException("Unknown model class: $modelClass")
            }
        } as T
    }
}